---
title: Xiomara CTF 2017 - Easy Login - 50
layout: default
author: Hank
avatar: hank.png
comments: true
tags: web
---

## Challenge:<br>
<font style="color:red">An aspiring engineer started learning web development on Youtube a day ago and he was asked to build a nice, secure, simple login page as part of his project. Well, he just started off so don't blame him. Go, hack!<br>
http://139.59.61.220:23478/    <br></font>
<br>
First take a quick look in html source code<br>
![]({{site.images}}/1.1.png)<br>
<br>
==> flag.css ??? Bla bla bla I do not care about this file<br>
==> main.js ???<br>
<br>
![]({{site.images}}/1.png)<br>
<br>
I have user and password :)<br>
But it not necessery because after that is a html page "secureflag.html"
==> http://139.59.61.220:23478/secureflag.html<br>
<br>
I got an image<br>
<br>
![]({{site.images}}/hiddenflag.jpeg)<br>
<br>
Using HxD to analytics<br><br>
![]({{site.images}}/2.png)<br>
<br>
## FLAG : xiomara{50_y0u_ar3_@_h@ck3r}<br>
<br>