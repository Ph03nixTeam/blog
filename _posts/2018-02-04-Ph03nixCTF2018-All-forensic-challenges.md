---
title: "[Ph03nixCTF 2018] All forensic challenges"
layout: default
author: BlackWings
avatar: bw.png
tags: forensic
comments: true
---

# **When February 3rd 2018 from 2:00AM to 2:00pm UTC +0, we organized a CTF contest in Vietnam. Now contest ended and this is my solution for all forensic challenges.**
<br>
# **I) Dưa Hành - 200 point - Author: BlackWings**
![](http://gitlab.com/Ph03nixTeam/images/raw/master/Screen Shot 2018-02-05 at 12.36.11 AM.png)<br>
Back up: [dump.pcapng-b82b54086dead53419d55e264821fd19](https://github.com/Ph03nixTeam/Ph03nixCTF-2018-challenges/blob/master/Forensic/dump.pcapng-b82b54086dead53419d55e264821fd19?raw=true)<br>
<br>
<br>
You can follow TCP in wireshark but you will see something wrong. That because every packet has been shuffled, so you must reorder packet and something cool will be appear.<br>
To do that I use tcpflow to reorder
```shell
tcpflow -d2 -r dump.pcapng-b82b54086dead53419d55e264821fd19
file 192.168.000.013.01337-192.168.000.037.01337
```
![](http://gitlab.com/Ph03nixTeam/images/raw/master/Screen Shot 2018-02-05 at 12.47.57 AM.png)<br>
<br>
![](http://gitlab.com/Ph03nixTeam/images/raw/master/192.168.000.013.01337-192.168.000.037.01337.jpg)<br>
```
FLAG: Ph03nix{L0L_K@m3h@m3h@...!!!...!!!...!!!}
```
<br>
<br>
<br>
<br>
<br>
# **II) Chả lụa - 300 point - Author: BlackWings**
![](http://gitlab.com/Ph03nixTeam/images/raw/master/Screen Shot 2018-02-05 at 12.38.24 AM.png)<br>
Back up: [image.png-0bc0d594a317cb5175a917ed10b367bb](https://github.com/Ph03nixTeam/Ph03nixCTF-2018-challenges/blob/master/Forensic/image.png-0bc0d594a317cb5175a917ed10b367bb?raw=true)<br>
<br>
Let use an eye drops before see that picture ![](https://gitlab.com/Ph03nixTeam/images/raw/master/smile/smiles28.png)![](https://gitlab.com/Ph03nixTeam/images/raw/master/smile/smiles28.png)![](https://gitlab.com/Ph03nixTeam/images/raw/master/smile/smiles28.png)
<br>
You will realize that picture was made from 100 same pictures, and you can check some sample with this code
<br>
```python
from PIL import Image

img = Image.open('image.png-0bc0d594a317cb5175a917ed10b367bb')
print img.size
img1 = img.crop((0,0,1280/10,1020/10))
img2 = img.crop((128*3,102*3,128*4,102*4))

print str(list(img2.getdata()))

print str(list(img1.getdata()))==str(list(img2.getdata()))
```
![](https://gitlab.com/Ph03nixTeam/images/raw/master/Screen Shot 2018-02-05 at 1.08.21 AM.png)<br>
And image data look same ascii ??? Try and im sure that is base64 but something not true in here ![](https://gitlab.com/Ph03nixTeam/images/raw/master/smile/smiles30.png)![](https://gitlab.com/Ph03nixTeam/images/raw/master/smile/smiles30.png)![](https://gitlab.com/Ph03nixTeam/images/raw/master/smile/smiles30.png)
<br>
```python
from PIL import Image
from base64 import b64decode, b64encode
img = Image.open('image.png-0bc0d594a317cb5175a917ed10b367bb')
img1 = img.crop((0,0,1280/10,1020/10))

s=''
for i in list(img1.getdata()):
  for j in i:
    s+= chr(j)

for i in range(0,len(s),4):
  current = s[i:i+4]
  print b64encode(b64decode(current)) == current
```
<br>
![](https://gitlab.com/Ph03nixTeam/images/raw/master/Screen Shot 2018-02-05 at 1.22.02 AM.png)<br>
Hmm, Why False??? Maybe a message has been hidden on this, that is my solution for compare and realize hidden message:<br>
```python
from PIL import Image
from base64 import b64decode, b64encode
from Crypto.Util.number import long_to_bytes

def get_base64_diff_value(s1, s2):
 base64chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
 res = 0
 for i in xrange(len(s1)):
  if s1[i] != s2[i]:
   return abs(base64chars.index(s1[i]) - base64chars.index(s2[i]))
 return res

img = Image.open('image.png-0bc0d594a317cb5175a917ed10b367bb')
img1 = img.crop((0,0,1280/10,1020/10))

s=''
for i in list(img1.getdata()):
  for j in i:
    s+= chr(j)

bin_str = ''
for i in range(0,len(s),4):
  steg_line = s[i:i+4]
  norm_line = b64encode(b64decode(steg_line))
  diff = get_base64_diff_value(steg_line, norm_line)

  pads_num = steg_line.count('=')
  if diff:
   bin_str += bin(diff)[2:].zfill(pads_num * 2)
  else:
   bin_str += '0' * pads_num * 2
	 
print bin_str
print long_to_bytes(int(bin_str,2))
```
<br>
![](https://gitlab.com/Ph03nixTeam/images/raw/master/Screen Shot 2018-02-05 at 1.34.21 AM.png)<br>
<br>
```shell
FLAG: Ph03nix{RFC4648_I_l@v@_y0u_ch1u_ch1u_ch1u!!!!}
```
<br>
# **Thank for your participation! See you again in next Ph03nix CTF Contest!**
![](https://gitlab.com/Ph03nixTeam/images/raw/master/smile/smiles40.png)![](https://gitlab.com/Ph03nixTeam/images/raw/master/smile/smiles40.png)![](https://gitlab.com/Ph03nixTeam/images/raw/master/smile/smiles40.png)![](https://gitlab.com/Ph03nixTeam/images/raw/master/smile/smiles40.png)![](https://gitlab.com/Ph03nixTeam/images/raw/master/smile/smiles40.png)![](https://gitlab.com/Ph03nixTeam/images/raw/master/smile/smiles40.png)![](https://gitlab.com/Ph03nixTeam/images/raw/master/smile/smiles40.png)
