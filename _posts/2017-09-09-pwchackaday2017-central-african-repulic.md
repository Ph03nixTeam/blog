---
title: "[PwC] Central African Repulic - MISC150"
layout: default
author: BlackWings
avatar: bw.png
comments: true
tags: misc
---

# Question<br>
![]({{site.images}}/Screen Shot 2017-09-09 at 10.21.08 PM.png)
<br>
PCAP FILE:<br> [https://github.com/Ph03Nix-MrV/PwCHackaday2017/blob/master/5c7d7800965f7012b2adde87fc479d62.pcap](https://github.com/Ph03Nix-MrV/PwCHackaday2017/blob/master/5c7d7800965f7012b2adde87fc479d62.pcap)<br>
<br>
Follow DNS protocol and first packet have FFD8<br>
<br>
![]({{site.images}}/Screen Shot 2017-09-09 at 10.26.11 PM.png)<br>
<br>
==> Maybe this is a JPG image. Huh try to recover...<br>
<br>
```python
from scapy.all import *
 from libnum import *
 packets = rdpcap('5c7d7800965f7012b2adde87fc479d62.pcap')
 i=0
 s=''
 for p in packets:
   if p.haslayer(DNS):
     if p.qdcount > 0 and isinstance(p.qd, DNSQR):
       name = p.qd.qname
     elif p.ancount > 0 and isinstance(p.an, DNSRR):
       name = p.an.rdata
     else:
       continue
     if i%2==0:
      try:
       n2s(int(name.replace('.g00gle.com.',''),16))
       s+=name.replace('.g00gle.com.','')
      except:
       continue
     i+=1
 s=n2s(int(s,16))
 open('a.jpg','w').write(s)
 ```
 <br>
<br>
a.jpg: [https://github.com/Ph03Nix-MrV/PwCHackaday2017/blob/master/a.jpg](https://github.com/Ph03Nix-MrV/PwCHackaday2017/blob/master/a.jpg)<br>
<br>
and this is a normal JPG but in the end I see some offset<br>
<br>
![]({{site.images}}/Screen Shot 2017-09-09 at 10.03.48 PM.png)<br>
<br>
![]({{site.images}}/Screen Shot 2017-09-09 at 10.32.37 PM.png)<br>
<br>
