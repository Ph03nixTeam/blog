---
title: SEC-T CTF 2017 - WEB - Sprinkler System(100p)
layout: default
author: Hank
comments: true
tags: web
avatar: hank.png
---

Long time no ctffffff,<br>
CTF: SEC-T CTF 2017<br>
Challenge: Sprinkler System<br>
Classification: Web<br>
![]({{site.images}}/Screen Shot 2017-09-15 at 10.11.16 PM.png)<br>
<br>
Then we go to the site:<br>
![]({{site.images}}/Screen Shot 2017-09-15 at 10.10.05 PM.png)<br>
<br>
Nothing much in here, so let's check out robots.txt.<br>
![]({{site.images}}/Screen Shot 2017-09-15 at 10.09.05 PM.png)<br>
<br>
So i type "/cgi-bin/test-cgi" into the browser, it gave me information about the test report.<br>
![]({{site.images}}/Screen Shot 2017-09-15 at 10.09.02 PM.png)<br>
<br>
I check out the google with keywords: " test-cgi exploit " it gave me the information about the [CVE-1999-0070](https://www.exploit-db.com/exploits/20435/)<br>
 Apache 0.8.x/1.0.x / NCSA httpd 1.x - test-cgi Directory Listing. Inputting "/cgi-bin/test-cgi?\*" shows the scripts that are present for this site, and we can see something sprinkler-related.<br>
![]({{site.images}}/Screen Shot 2017-09-15 at 10.08.55 PM.png)<br>
<br>
 Now let's go to enable_sprinkler_system to see what inside it:<br>
![]({{site.images}}/Screen Shot 2017-09-15 at 10.08.51 PM.png)<br>
## Haha we have the flag: SECT{-p00l_On_t3h_r00f_must_h@v3_A_l3ak!-} !<br>
<br>