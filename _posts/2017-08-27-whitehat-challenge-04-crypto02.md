---
title: "[WhiteHat Challenge 04] crypto02"
layout: default
author: BlackWings
avatar: bw.png
comments: true
tags: crypto
---

# Question
```
Find flag in http://chall04-crypto02.wargame.whitehat.vn
```
<br>
We have a script:<br>
![]({{site.images}}/Screen Shot 2017-08-27 at 6.26.42 PM.png)<br>
<br>
With that I can know AES ECB mode and try to find len(flag)<br>
<br>
![]({{site.images}}/Screen Shot 2017-08-27 at 6.32.59 PM.png)<br>
<br>
When input 8 bytes I have 3 block cipher and 2 block for 7 bytes so I can calculate len(flag)<br>
<br>
```
len('something!') + 8 + len(flag) + 16 bytes padding = 3*16  
 ==> len(flag) = 14
```
<br>
Because blocks have same encryption so I can use padding to bruteforce flag<br>
<br>
Block 1: "something!" + 'a'\*37 + 1 character bruteforce<br>
Block 2: "something!" + 'a'\*37 + flag[0]<br>
<br>
Repeat like that I got flag ![]({{site.images}}/smile/smiles79.png)![]({{site.images}}/smile/smiles79.png)![]({{site.images}}/smile/smiles79.png)<br>
<br>
![]({{site.images}}/Screen Shot 2017-08-27 at 6.53.35 PM.png)<br>
<br>
![]({{site.images}}/Screen Shot 2017-08-27 at 6.43.38 PM.png)<br>
<br>
Run babe runnnnnnn =]]]]]]]]]<br>
<br>
##  Flag: [\*] WhiteHat{1ffe5f92c181a89dd267cfd4b6ec2c80c6202391} [\*]  <br>
<br>