---
title: WhiteHat Grand Prix 2016 - Web100 - Bánh Bột Lọc
layout: default
author: BlackWings
avatar: bw.png
comments: true
tags: web
---

## Challenge:<br>
[http://web05.grandprix.whitehatvn.com](http://web05.grandprix.whitehatvn.com)<br>
<br>
Step 1: View source index.php.bak<br>
```php
	$key="1337";
	if ($username.$key == md5($password)){
		echo $secret;
	} 
```
		
Step 2: We must Brute-force password, I write a python script to solve that problem<br>
[BruteForce.py](https://github.com/Ph03Nix-MrV/Whitehatvn-Grandprix-2016/blob/master/Web100/Banh-Bot-Loc/bruteforce.py)<br>
<br>
Step 3: Submit flag<br>
[http://web05.grandprix.whitehatvn.com/index.php?username=3acc36691f4866c6654ce9f8bdfc&password=BVCA](http://web05.grandprix.whitehatvn.com/index.php?username=3acc36691f4866c6654ce9f8bdfc&password=BVCA)<br>
<br>
## FLAG: WhiteHat{92ab818618fee438a1ea3944b5940237975f2b1d}<br>
<br>
<br>