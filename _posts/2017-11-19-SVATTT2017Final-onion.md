---
title: "[SVATTT2017 Final] onion"
layout: default
author: BlackWings
avatar: bw.png
tags: crypto
comments: true
---

# **QUESTION**
[onion.zip](https://github.com/Ph03Nix-MrV/SVATTT2017Final/blob/master/onion.zip)

Idea of encrytion: <br>
FLAG ==> process ==> transform ==> enlarge ==> generate_equation<br>
<br>
Take quick look on process() i saw FLAG have been splited to 64 blocks and length of last block can < 9, length of other blocks == 9<br>
==> maximum of value is 2^9 == 512<br>
<br>
And after transform() value == value + 0x636f<br>
So I bruteforce couple of x, y based on each equation in solveme.txt<br>
<br>
```python
f=open('solveme.txt').read()

ch = []
arr = f.split('\n\n')[:-1]

for line in arr:
  F(x,y) = line.split(' == ')[0]
  X = Integer(line.split(' == ')[1])
  c=False
  for i in range(512):
    for j in range(512):
      a = i+ 0x636f
      b = j+ 0x636f
      S = F(a,b)
      if Integer(S) ==Integer(X):
        ch.append(i)
        ch.append(j)
        c=True
        break
    if c:
      print ch
      break
```
Smoke a few cigarettes and waiting magic come ![]({{site.images}}/smile/smiles83.png)<br><br><br>
![]({{site.images}}/smile/smiles18.png)![]({{site.images}}/smile/smiles18.png)![]({{site.images}}/smile/smiles18.png)<br><br><br>
![]({{site.images}}/Screen Shot 2017-11-19 at 9.40.09 AM.png)<br><br>
<br>
Continue bruteforce raw binary of FLAG and solve problem :D<br>
<br>
```python
from Crypto.Util.number import long_to_bytes

def transform(byte, n):
    return (byte >> n) | ((byte & (((3 * (n * 2 + 1) & 1) << n) - (7 * (n * 2 + 1) & 1))) << (size - n))
def process(FLAG):
    flag = int(FLAG.encode('hex'), 16)
    flag = bin(flag).lstrip('0b')
    FLAG = []
    for i in range(0, len(flag), size):
        FLAG.append(int(flag[i:i + size], 2))
    return FLAG

size = 9

arr=[361, 293, 84, 138, 197, 419, 189, 404, 216, 177, 383, 432, 344, 366, 87, 273, 419, 229, 254, 225, 349, 438, 140, 205, 107, 498, 163, 249, 206, 220, 105, 311, 315, 475, 44, 55, 355, 261, 434, 216, 249, 303, 60, 150, 493, 19, 291, 297, 184, 220, 206, 360, 303, 218, 301, 390, 343, 411, 420, 416, 70, 288, 400, 335]

A=[]
for i in range(64):
  for j in range(512):
    x = transform(j, i % size)
    x = transform(x, (i + 3) % size)
    if x == arr[i]:
      A.append(j)

s=''
for i in A[:-1]:
  tmp = bin(i)[2:]
  if len(tmp) < size:
    tmp = '0'*(size-len(tmp)) + tmp
  s+=tmp

print 'case 1: ',long_to_bytes(int(s+bin(A[-1])[2:],2))
print 'case 2: ',long_to_bytes(int(s+'0'+bin(A[-1])[2:],2))
print 'case 3: ',long_to_bytes(int(s+'00'+bin(A[-1])[2:],2))

```
<br>
![]({{site.images}}/Screen Shot 2017-11-19 at 9.48.20 AM.png)
<br><br>
![]({{site.images}}/smile/smiles83.png)![]({{site.images}}/smile/smiles83.png)![]({{site.images}}/smile/smiles83.png)<br><br><br>
## **SCRIPT:**<br>
[brute.sage](https://github.com/Ph03Nix-MrV/SVATTT2017Final/blob/master/brute.sage)<br>
[solve.py](https://github.com/Ph03Nix-MrV/SVATTT2017Final/blob/master/solve.py)<br><br>
## **FLAG: SVATTT{Wel1_secur1ty_1s_l1k3_4n_0nion_actually_0r_m0re...an_illusi0n!!!}**
