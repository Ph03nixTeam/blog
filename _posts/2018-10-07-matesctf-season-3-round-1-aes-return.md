---
title: "[MATESCTF SS3 ROUND 1] AES Return"
layout: default
author: BlackWings
avatar: bw.png
tags: crypto
comments: true
---

# QUESTION
```python
import os
import binascii

s_box = [0x00] * 256

def sub_bytes(s):
    for i in range(4):
        for j in range(4):
            s[i][j] = s_box[s[i][j]]

def shift_rows(s):
    s[0][1], s[1][1], s[2][1], s[3][1] = s[1][1], s[2][1], s[3][1], s[0][1]
    s[0][2], s[1][2], s[2][2], s[3][2] = s[2][2], s[3][2], s[0][2], s[1][2]
    s[0][3], s[1][3], s[2][3], s[3][3] = s[3][3], s[0][3], s[1][3], s[2][3]

def add_round_key(s, k):
    for i in range(4):
        for j in range(4):
            s[i][j] ^= k[i][j]


xtime = lambda a: (((a << 1) ^ 0x1B) & 0xFF) if (a & 0x80) else (a << 1)

def mix_single_column(a):
    # see Sec 4.1.2 in The Design of Rijndael
    t = a[0] ^ a[1] ^ a[2] ^ a[3]
    u = a[0]
    a[0] ^= t ^ xtime(a[0] ^ a[1])
    a[1] ^= t ^ xtime(a[1] ^ a[2])
    a[2] ^= t ^ xtime(a[2] ^ a[3])
    a[3] ^= t ^ xtime(a[3] ^ u)


def mix_columns(s):
    for i in range(4):
        mix_single_column(s[i])

r_con = (
    0x00, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40,
    0x80, 0x1B, 0x36, 0x6C, 0xD8, 0xAB, 0x4D, 0x9A,
    0x2F, 0x5E, 0xBC, 0x63, 0xC6, 0x97, 0x35, 0x6A,
    0xD4, 0xB3, 0x7D, 0xFA, 0xEF, 0xC5, 0x91, 0x39,
)


def bytes2matrix(text):
    """ Converts a 16-byte array into a 4x4 matrix.  """
    return [list(text[i:i+4]) for i in range(0, len(text), 4)]

def matrix2bytes(matrix):
    """ Converts a 4x4 matrix into a 16-byte array.  """
    return bytes(sum(matrix, []))

def xor_bytes(a, b):
    """ Returns a new byte array with the elements xor'ed. """
    return bytes(i^j for i, j in zip(a, b))

def pad(plaintext):
    """
    Pads the given plaintext with PKCS#7 padding to a multiple of 16 bytes.
    Note that if the plaintext size is a multiple of 16,
    a whole block will be added.
    """
    padding_len = 16 - (len(plaintext) % 16)
    padding = bytes([padding_len] * padding_len)
    return plaintext + padding

class AES(object):
    def __init__(self, master_key, sbox = 12558969026229242650325497035900771229425977298929824185180201595218723846521641084878950437814262613818218678855083246390579973265062862098835367276463213485935490075333543022908418188023689628239853057430766867816104756072163930330964533854567180021957297209556449520724782473372358521936890038196263501971865464205202080665440031474485076128621298987969793026051666299271938394305982997346692361070006228877008335903374077985233742319271830419611735823883778589260764256621910077884203694152337712033528307796362819892376495515250925183618990912197919529504741016040626997946257218207277210591984163100337150147350):
        global s_box
        tmp = str(hex(sbox).replace('0x','')).zfill(512)
        s_box = [int(tmp[i:i+2],16) for i in range(0,len(tmp),2)]
        self.n_rounds = 10
        self._key_matrices = self._expand_key(master_key)

    def _expand_key(self, master_key):
        key_columns = bytes2matrix(master_key)
        iteration_size = len(master_key) // 4

        columns_per_iteration = len(key_columns)
        i = 1
        while len(key_columns) < (self.n_rounds + 1) * 4:
            word = list(key_columns[-1])

            if len(key_columns) % iteration_size == 0:
                word.append(word.pop(0))
                word = [s_box[b] for b in word]
                word[0] ^= r_con[i]
                i += 1
            elif len(master_key) == 32 and len(key_columns) % iteration_size == 4:
                word = [s_box[b] for b in word]

            word = xor_bytes(word, key_columns[-iteration_size])
            key_columns.append(word)

        return [key_columns[4*i : 4*(i+1)] for i in range(len(key_columns) // 4)]

    def encrypt_block(self, plaintext):
        assert len(plaintext) == 16
        plain_state = bytes2matrix(plaintext)

        add_round_key(plain_state, self._key_matrices[0])

        for i in range(1, self.n_rounds):
            sub_bytes(plain_state)
            shift_rows(plain_state)
            mix_columns(plain_state)
            add_round_key(plain_state, self._key_matrices[i])

        sub_bytes(plain_state)
        shift_rows(plain_state)
        add_round_key(plain_state, self._key_matrices[-1])

        return matrix2bytes(plain_state)

    def encrypt_cbc(self, plaintext):
        iv = os.urandom(16)
        plaintext = pad(plaintext)
        blocks = []
        previous = iv
        for i in range(0, len(plaintext), 16):
            plaintext_block = plaintext[i:i+16]
            block = self.encrypt_block(xor_bytes(plaintext_block, previous))
            blocks.append(block)
            previous = block
        return binascii.hexlify(iv + b''.join(blocks))

file = open('flag.txt','r').read().encode('ascii')
challenge = '''To make this challenge more easy,
I'll give you the encrypted flag: {flag}
Have fun!'''
menu = '''1. encrypt
2. decrypt
3. go pro
'''
query = 5

if __name__ == '__main__':
    key = os.urandom(16)
    aes = AES(key)
    print(challenge.format(flag = str(aes.encrypt_cbc(file),'ascii')))
    while query:
        try:
            inp = input(menu)
            if inp == '1':
                plaintext = input("Give me something: ")
                ciphertext = aes.encrypt_cbc(plaintext.encode('ascii'))
                print('Here you are ' + str(ciphertext,'ascii'))
            elif inp == '2':
                print("TO DO!")
            elif inp == '3':
                plaintext = input("Give me something: ")
                aes = AES(key, int(plaintext, 10))
                ciphertext = aes.encrypt_cbc(file)
                print('Here you are ' + str(ciphertext,'ascii'))
            else:
                print("Bye!")
                break
            query -= 1
        except ValueError:
            print('You can not do that!')
            break
```

Connect and get flag cipher
```python
FLAG CIPHER: c6c87908b972e20af88175ea46785459c2ffef77d700db0912eb9169042cabf0e8fa72b85bd4e74f04c4bc05436ba7dec6065a19fba052142e5ad2c94308e178
```
AES MODE CBC, Hmm... bit flipping?... padding oracle?...<br>
Nope to solve that challenge, I need reverse key.<br>
To do that, take a look on ```__init__```, prepare sbox from user input, expand key, bla bla...<br>
So what happend when sbox = [0] * 256<br>

```python
FLAG CIPHER: d80db7681e92c4838fbae619f7343e367af01e5f63a0b26966b8915498027a677af01e5f63a0b26966b8915498027a677af01e5f63a0b26966b8915498027a67
```
==> According theory if sbox = [0] * 256 result of encrypt MODE CBC will be same with MODE ECB<br>

But not easy like theory, still IV and at round 1 of AES expand key process plaintext block will be xored with IV<br>

sbox = [0] * 256 that mean sub_bytes, shift_rows and mix_columns will be [0] * 16<br>

```python
def add_round_key(s, k):
    for i in range(4):
        for j in range(4):
            s[i][j] ^= k[i][j]
```

==> plaintext block xored with ROUND KEY then substitute, shift, mix. But result from that is [0] * 16 so at the last loop ciphertext will be ROUND KEY ==> Key has been leaked<br>
![]({{site.images}}/smile/smiles10.png)![]({{site.images}}/smile/smiles44.png)![]({{site.images}}/smile/smiles28.png)
<br><br><br>

With last ROUND KEY, calculate KEY and submit flag<br>

```python
def xor(a,b):
    return [ord(x) ^ ord(y) for x, y in zip (a,b)]


def gen_key_col(xxx, key_col):
    rs =[]
    rs.append(xxx)
    for i in range(1,11):
        xxx = xor(''.join(chr(x) for x in xxx),
                    ''.join(chr(x) for x in key_col[i]))
        rs.append(xxx)
    return rs


def unpad(plaintext):
    return plaintext[:len(plaintext) - ord(plaintext[-1])]


r_con = [0x00, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1B, 0x36]
flag = 'c6c87908b972e20af88175ea46785459c2ffef77d700db0912eb9169042cabf0e8fa72b85bd4e74f04c4bc05436ba7dec6065a19fba052142e5ad2c94308e178'
IV_flag = flag[:32]
flag = flag[32:]
cipher = 'd80db7681e92c4838fbae619f7343e367af01e5f63a0b26966b8915498027a677af01e5f63a0b26966b8915498027a677af01e5f63a0b26966b8915498027a67'
cipher = cipher[-32:].decode('hex')
cipher = [ord(x) for x in cipher]
key = [0]*16

'''
key[0:4]
'''
key[0] = cipher[0]
for i in r_con[::-1]:
    key[0] ^= i
key[1] = cipher[1]
key[2] = cipher[2]
key[3] = cipher[3]
key_col = []
key_col.append([x for x in key[:4]])
for i in range(1, 11):
    tmp = xor(''.join(chr(x) for x in [r_con[i], 0, 0, 0]),
                        ''.join(chr(x) for x in key_col[-1]))
    key_col.append(tmp)

'''
key[4:]
'''
for pos in range(4,16):
    if pos == 8 or pos == 12:
        key_col = gen_key_col(key[pos-4:pos],key_col)
    for c in range(256):
        ch = c
        for part in key_col[1:]:
            ch = ch ^ part[pos%4]
        if ch == cipher[pos]:
            key[pos] = c

from Crypto import Random
from Crypto.Cipher import AES
key = ''.join(chr(x) for x in key)
IV = IV_flag.decode('hex')
aes = AES.new(key,AES.MODE_CBC,IV)
print unpad(aes.decrypt(flag.decode('hex')))

```

# FLAG <br>
# MATESCTF{D0nt_Ch4ng3_5B0x_V4lu3_3v3r!}