---
title: "[SVATTT2017] crypto300"
layout: default
tags: crypto
author: BlackWings
avatar: bw.png
comments: true
---

## **QUESTION** *(đã tự ý sửa flag, xor_key random):*

[server.py](https://raw.githubusercontent.com/Ph03Nix-MrV/SVATTT2017/master/server.py) <br />
[secret.py](https://raw.githubusercontent.com/Ph03Nix-MrV/SVATTT2017/master/secret.py) <br />
[KEY.txt](https://raw.githubusercontent.com/Ph03Nix-MrV/SVATTT2017/master/KEY.txt) <br />

Mô hình bài này như sau
+ plaintext = b64decode(b64decode(string) ^ [ xor_key* (N lần) + padding ] )
+ plaintext có dạng json string

Lần mò server.py có cái này
```python
if len(key) != 64:
 rsend("Invalid key!\n")
```
 ==> len(XOR_KEY) == 64
```python
if len(pin) >= 4:
 pin = pin[:4]
 ```
==> input sẽ bị cắt còn tối đa 4 kí tự hoy

Ý tưởng tương tự **CVE-2017-9248**<br>
b64decode(cipher) ^ XOR_KEY phải là 1 chuỗi base64 hợp lệ ==> phải chia hết cho 4<br>
len(XOR_KEY) == 64 ==> bruteforce theo block 4 byte => tất cả có 64/4 = 16 block<br>
request đại để tính 4 byte cuối của b64decode(cipher) ^ XOR_KEY nằm trong block nào
```python
def getcipher():
 r.send('1\n')
 r.recv()
 r.send('1234\n')
 time.sleep(0.1)
 res = r.recv().split('\n')[0].split(':')[1].strip()
 return res
def getBlock():
 b64 = getcipher()
 time.sleep(0.3)
 s = b64decode(b64)
 s= s[(len(s)/64)*64:]
 b = (len(s)+1)/4-1
 if b == -1:
 b += 16
 return b, b64decode(b64)[-4:]
```
biết block rầu giờ decrypt như nào đây ???



Để ý chút xíu plaintext ở đây là JSON string => kết thúc string bằng dấu "}"<br>
Vậy đơn giản hoy bruteforce key => decrypt thử coi có "}" khồng<br>
Nhưng để làm được cần ít nhất 2 đoạn cipher để check =)) lại request bạo lực thôi (Lần này nhắm mục tiêu vô thằng block vừa tính )

![]({{site.images}}/Screen%20Shot%202017-11-06%20at%204.45.01%20AM.png)

Lẹ lẹ nào con bích :(

![]({{site.images}}/Screen%20Shot%202017-11-06%20at%204.49.17%20AM.png)

Tình hình là thi không tốt nên chán chẳng muốn viết nữa các đồng chí ạ :(<br>
Thoai mời các đồng chí ngồi đọc script vầy :(<br>
[solve.py](https://raw.githubusercontent.com/Ph03Nix-MrV/SVATTT2017/master/solve.py)

 <h2 style="color: red;">SVATTT2017{kh0n9_du0c_du_l1ch_D4_N4NG_r01_CVE_2017_9248}</h2>
