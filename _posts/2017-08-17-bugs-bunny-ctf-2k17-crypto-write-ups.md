---
title: "[bugs_bunny ctf 2k17] Crypto Write-Ups"
layout: default
author: BlackWings
avatar: bw.png
comments: true
tags: crypto
---

### Sorry everyone, because when this CTF event running, i busy. Now i'm free and solved some challenges.<br>
# <font style="color:Crimson  ">Crypto-15</font><br>
![]({{site.images}}/Capture.PNG)<br><br>
This is Shift Cipher, you can use [this tool](https://www.xarg.org/tools/caesar-cipher/) or write some lines of code to solve [crypto-15.py](https://github.com/Ph03Nix-MrV/bugs_bunny-ctf-2k17/blob/master/crypto-15.py)<br>
## [\*] Decrypted text: Bugs_Bunny{C35aR_3NC0D3_4R3_N0T_S3CuR3_AT_4LL} <br>
## Rot : 25<br>
<br>
<br>
# <font style="color:Crimson  ">Crypto-20</font><br>
![]({{site.images}}/Capture1.PNG)<br>
<br>
Do you know brainfuk languague? Use [this tool](http://www.dcode.fr/brainfuck-language) to decode<br>
## FLAG: Bugs_Bunny{Br41N_Fu\*\*}<br>
<br>
<br>
# <font style="color:Crimson  ">Crypto-25</font><br>
## Question: [crypto-25](https://github.com/Ph03Nix-MrV/bugs_bunny-ctf-2k17/blob/master/crypto-25)<br>
Another eazy challenges 😋😋😋 just open this file with notepad, this is Ook language. Decode with [this tool](http://www.dcode.fr/ook-language)<br>
## FLAG: Bugs_Bunny{Ju5T_OoK!}<br>
<br>
<br>
# <font style="color:Crimson  ">Scy way</font><br>
![]({{site.images}}/Capture2.PNG)<br>
Question name told me everything Scy ==> Scytale Cipher<br>
Solved with [this tool](http://www.dcode.fr/scytale-cipher)<br>
 FLAG: Bugs_Bunny{ISHOULDLEARNMORECIPHER}<br>
<br><br>
# <font style="color:Crimson  ">Crypto-50</font><br>
Question:  [enc-crypto-50.txt](https://github.com/Ph03Nix-MrV/bugs_bunny-ctf-2k17/blob/master/enc-crypto-50.txt)<br>
 Hmm, this is base64 encode but when i decode result is another base64 encode. I know flag have 'Bugs_Bunny' so solved with some lines of code ✊✊✊ [crypto-50.py](https://github.com/Ph03Nix-MrV/bugs_bunny-ctf-2k17/blob/master/crypto-50.py)<br>
## FLAG: Bugs_Bunny{N0T_H4Rd_4T_4ll}<br>
<br><br>
# <font style="color:Crimson  ">Baby RSA</font><br>
![]({{site.images}}/Capture3.PNG)<br>
<br>
This is basic RSA, use FactorDB to find p, q<br>
[http://factordb.com/index.php?query=20473673450356553867543177537](http://factordb.com/index.php?query=20473673450356553867543177537)<br>
<br>
P = 2165121523231<br>
Q =  9456131321351327<br>
phi = ( P - 1) * ( Q - 1)<br>
==> D ==> Decrypted message<br>
[Baby-RSA.py](https://github.com/Ph03Nix-MrV/bugs_bunny-ctf-2k17/blob/master/Baby-RSA.py)<br>
## FLAG: Bugs_Bunny{Baby_RSA_Its_Cool_Lik3_school_haHAha}<br>
<br>
<br>
# <font style="color:Crimson  ">RSA2</font><br>
## Question: [RSA2.txt](https://github.com/Ph03Nix-MrV/bugs_bunny-ctf-2k17/blob/master/RSA2.txt)<br>
<br>
FactorDB cannot find P, Q so we need to use another algorithm to find P, Q. I use Wiener's attack to find P, Q<br>
[https://github.com/rk700/attackrsa](https://github.com/rk700/attackrsa)<br>
```
attackrsa -t wiener -n N -e E
```

## FLAG:  Bugs_Bunny{Baby_Its_Cool_Lik3_school_haHAha}<br>
<br>
<br>