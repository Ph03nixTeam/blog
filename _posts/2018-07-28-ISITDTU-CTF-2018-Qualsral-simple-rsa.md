---
title: "[ISITDTU CTF 2018 Quals] Simple RSA"
layout: default
author: BlackWings
avatar: bw.png
tags: crypto
comments: true
---

# Long time no CTF and tonight my friend ask me about this challange so LET'S PLAY FOR FUN !<br>![]({{site.images}}/smile/smiles18.png)![]({{site.images}}/smile/smiles18.png)![]({{site.images}}/smile/smiles18.png)<br><br><br>
# QUESTION
[https://bit.ly/2LDPBtM](Download: https://bit.ly/2LDPBtM)
<br><br><br>

Take a look to understand how to generate N

```
p = random.randint(1<<251,1<<252)
p = next_prime(p)
p1 = next_prime(p*10)
p2 = next_prime(p1*10)
p3 = next_prime(p2*10)

N = p*p1*p2*p3
```

Break the math rule and I see N &asymp; pow(10, 3)*  pow(p, 4)<br>
==>THAT IS TOO ENOUGH!!!<br><br><br>
## WITH BINARY SEARCH ALGORITHM WE CAN EASILY FIND p<br>
![]({{site.images}}/smile/smiles10.png)![]({{site.images}}/smile/smiles10.png)![]({{site.images}}/smile/smiles10.png)<br><br><br>
```
from Crypto.Util.number import *
from libnum import *
from math import sqrt

def dec_prime(n):
  num = n
  while True:
    if isPrime(num):
      return num
    num -= 1

def next_prime(n):
  num = n + 1
  while True:
    if isPrime(num):
      return num
    num += 1

N = 603040899191765499692105412408128039799635285914243838639458755491385487537245112353139626673905393100145421529413079339538777058510964724244525164265239307111938912323072543529589488452173312928447289267651249034509309453696972053651162310797873759227949341560295688041964008368596191262760564685226946006231

N_tmp  = N / 1000
p_tmp = int (sqrt(N_tmp))
p_tmp = int (sqrt(p_tmp))
max = p_tmp
min = p_tmp/10

while True:
  num = (max + min)/2
  p = dec_prime(num)
  p1 = next_prime(p*10)
  p2 = next_prime(p1*10)
  p3 = next_prime(p2*10)
  if N == p*p1*p2*p3:
    print 'p = ', p
    c = 153348390614662968396805701018941225929976855876673665545678808357493865670852637784454103632206407687489178974011663144922612614936251484669376715328818177626125051048699207156003563901638883835345344061093282392322541967439067639713967480376436913225761668591305793224841429397848597912616509823391639856132

    phi = (p-1)*(p1-1)*(p2-1)*(p3-1)
    e = 65537

    d = invmod(e,phi)

    pt = pow (c,d,N)
    print n2s(pt)
    break
  elif p*p1*p2*p3 - N > 0:
    max = num
  elif p*p1*p2*p3 - N < 0:
    min = num

```
<br><br>
## 1000 Thousand Years Later...
![]({{site.images}}/smile/smiles22.png)![]({{site.images}}/smile/smiles22.png)![]({{site.images}}/smile/smiles22.png)
<br><br><br>
```
p =  4955491002253862893875866857920361781272456756179979954923353247500965791683
ISITDTU{f6b2b7472273aacf803ecfe93607a914}
```
<br>
# FLAG ISITDTU{f6b2b7472273aacf803ecfe93607a914}