---
title: 3DSCTF 2016 - MISC100 - Santa walks into a bar
layout: default
author: BlackWings
avatar: bw.png
comments: true
tags: misc
---

## Challenge:<br>
<font style="color:red">
Santa walks into a bar and creates a friendship bound with you.<br>
After some shots, he spells to you his secrets to delivery all gifts on Christmas: he has a magical linked list that inform the next kiddie to visit.<br>
At the end of the night, he goes alway and left behind his wallet and the bag with the list of gifts to delivery. Try to discover if you will receive something.<br></font>
[a5d744fb06e04bacfde2e7b713054145.zip](https://github.com/Ph03Nix-MrV/3DSCTF2016/blob/master/MISC100/Santa%20walks%20into%20a%20bar/a5d744fb06e04bacfde2e7b713054145.zip)<br>
<br>

Step 1 \<Analytics>: I found start file from santa-id.png and every image in folder list is QR code<br>
Step 2 \<Testing> : Try decode some image we got<br>
```
	- Next name is XXXXX in YYYYYYYYYYYYY
	- A kid called XXXXX in YYYYYYYYYYYYY
	- Next kiddie is XXXXX in YYYYYYYYYYYYY
	- Now I have XXXXX in YYYYYYYYYYYYY
	- ...
```
Can you see something ?<br>
<br>
Step 3 : I follow YYYYYYYYYYYYY to find exception<br>
&nbsp;&nbsp;script: [solve.py](https://github.com/Ph03Nix-MrV/3DSCTF2016/blob/master/MISC100/Santa%20walks%20into%20a%20bar/solve.py)<br>
&nbsp;&nbsp;AND ...<br>
&nbsp;&nbsp;File: 3ab3b4b87d57315315cbb0259a262177.png <br>
&nbsp;&nbsp;QR decode: Y0ur gift is in goo.gl/wFGwqO inugky3leb2gqzjanruw42yk<br>
<br>
## FLAG : 3DS{I_h0p3_th4t_Y0u_d1d_n0t_h4v3_ch4ck3d_OnE_by_0n3}<br>
<br>
<br>