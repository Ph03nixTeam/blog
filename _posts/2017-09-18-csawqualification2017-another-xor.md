---
title: "[CSAW_Qualification_2017] Another Xor"
layout: default
author: BlackWings
avatar: bw.png
comments: true
tags: crypto
---

#  **Question:**<br>
![]({{site.images}}/Screen Shot 2017-09-18 at 9.16.18 PM.png)<br>
Backup: [Another_Xor_Question](https://github.com/Ph03Nix-MrV/CSAWQualification2017/tree/master/Crypto/Another_Xor_100)<br>
<br>
## Special thank for a member of MSEC team who told me about a special char in XOR_STRING and amazing idea came =]]<br>
<br>
[ PLAINTEXT + KEY + md5(PLAINTEXT + KEY) ] ^ [ XOR_STRING ] = CIPHERTEXT<br>
XOR_STRING  = KEY + KEY + KEY + ...<br>
len( PLAINTEXT + KEY + md5(PLAINTEXT + KEY) ) == len( CIPHERTEXT ) == 137<br><br>
First: I have a ^ b = c and c ^ b = a and flag has form 'flag{bla_bla_bla}' so with 5 first char of flag i can find 5 first char of key<br>
 flag{ ^ CIPHERTEXT[:5] = KEY[:5] = 'A qua'<br>
<br>
Second: Because md5 has 32 chars and each char in [a-z0-9] so i write a script to analytics all possible case for each char in XOR_STRING[-32:]<br>
<br>
![]({{site.images}}/Screen Shot 2017-09-18 at 9.28.34 PM.png)<br>
![]({{site.images}}/Screen Shot 2017-09-18 at 9.29.00 PM.png)<br>
![]({{site.images}}/Screen Shot 2017-09-18 at 9.29.14 PM.png)<br>
<br>
You can see in XOR_STRING[134] have 6 possible case {@, A, C, D, F, G} and one of that is 'A' but KEY[0] == 'A'<br>
<br>
So i think XOR_STRING = KEY*n + 'A q'<br>
But len(PLAINTEXT + KEY) == 137 - 32 == 105<br>
&nbsp;&nbsp;&nbsp;&nbsp;len(XOR_STRING) == 137<br>
&nbsp;&nbsp;&nbsp;&nbsp;len('flag{}') == 6<br><br>
 ==> 6 < len(KEY) < 91<br>
==> 1 < n < 22<br>
 &nbsp;&nbsp;&nbsp;&nbsp;but len(KEY) * n  == 134 it means 134 must divisible for n<br>
==> n == 2<br>
<br>
## FINAL STEP<br>
 len(KEY) == 134/2 == 67 and len(PLAINTEXT) + len(KEY) == 105<br>
==> len(PLAINTEXT) == 38<br>
<br>
```python
PLAINTEXT + KEY == 'flag{' + '_'*(38-6) + '}A qua' + '_'*(67-5)
XOR_STRING      == 'A qua' + '_'*(67-5) + 'A qua' + '_'*(67-5) + 'A q'
```
<br>
[Recover script: recover_plaintext.py](https://github.com/Ph03Nix-MrV/CSAWQualification2017/blob/master/Crypto/Another_Xor_100/recover_plaintext.py)<br>
<br>
![]({{site.images}}/Screen Shot 2017-09-18 at 9.57.56 PM.png)<br>
![]({{site.images}}/Screen Shot 2017-09-18 at 10.01.37 PM.png)<br>
<br>
#  FLAG: flag{sti11_us3_da_x0r_for_my_s3cratz}<br>
