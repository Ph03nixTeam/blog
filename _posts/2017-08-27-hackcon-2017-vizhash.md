---
title: "[HackCon 2017] VizHash"
layout: default
author: BlackWings
avatar: bw.png
comments: true
tags: crypto
---

# Question<br>
![]({{site.images}}/Screen Shot 2017-08-27 at 7.11.57 PM.png)<br>
<br>
ZIP file: [vizhash.zip](https://github.com/Ph03Nix-MrV/HackCon2017/blob/master/vizhash.zip)<br>
<br>
![]({{site.images}}/Screen Shot 2017-08-27 at 7.21.05 PM.png)<br>
<br>
With checksum and image data we can recover encrypted_flag<br>
<br>
![]({{site.images}}/Screen Shot 2017-08-27 at 7.34.17 PM.png)<br>
<br>
Finally I must decrypt encrypted_flag<br>
<br>
![]({{site.images}}/Screen Shot 2017-08-27 at 7.33.21 PM.png)<br>
<br>
![]({{site.images}}/Screen Shot 2017-08-27 at 7.35.13 PM.png)<br>
<br>
## Script [vizhash.py](https://github.com/Ph03Nix-MrV/HackCon2017/blob/master/vizhash.py)
<br><br>
# Flag: d4rk{no00000oo_not0x_myfaUltXXX}c0de  <br>
<br>