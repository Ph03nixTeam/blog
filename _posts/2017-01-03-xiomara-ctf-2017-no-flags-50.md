---
title: Xiomara CTF 2017 - No Flags? - 50
layout: default
author: Hank
avatar: hank.png
comments: true
tags: web
---

## Challenge:<br>
<font style="color:red">What would you do if we tell you there are no flags for this question? Go on, solve it. That reminds me, Nothing is impossible. <br>
http://139.59.61.220:23467/  <br></font>
<br>
Nothing on here, let's take a quick look at "robots.txt":<br>
```
User-agent:*
Disallow: /flags/
Disallow: /more_flags/
Disallow: /more_and_more_flags/
Disallow: /no_flag/  
```
<br>
When looking at the last one, we can see an interesting contrast to the other ones. We see weird characters, which are encoded by the following function:<br>
```js
function encode(str){
 str = str.replace(/http:/g, "^^^");
 str = str.replace(/bin/g, "*^$#!")
 str= str.replace(/com/g, "*%=_()");
 str= str.replace(/paste/g, "~~@;;");
} 
```
<br>
After decoding the string, we get [http://pastebin.com/SwzEKazp](http://pastebin.com/SwzEKazp). Hoping to find the flag there I directly jumped to the link but only faced disappointment, as I saw the text, that the page was removed. I googled a bit and came to the idea to use the [wayback machine](https://archive.org/web/), which was the correct answer. Searching the history revealed a snapshot with a base64 encoded string.<br> 
## Decode and finish: xiomara{1_4m_mr_r0b07}<br>
<br>
<br>